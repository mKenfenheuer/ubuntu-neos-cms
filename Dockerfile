FROM ubuntu

WORKDIR /

COPY install.sh /install.sh
RUN bash /install.sh
RUN rm /install.sh

COPY init.sh /init
RUN chmod +x /init
RUN chmod 755 /init

CMD ["/init"]