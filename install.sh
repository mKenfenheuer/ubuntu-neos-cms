#!/bin/bash

apt update
apt upgrade -y

apt install -y mysql-server

export DB_DATA_PATH="/data/database/"
export DB_ROOT_PASS="neos-cms-start123!"
export DB_USER="neoscms"
export DB_PASS="neoscms-secure123!"
export MAX_ALLOWED_PACKET="200M"

service mysql start

mysqladmin -u root password "${DB_ROOT_PASS}"

echo "GRANT ALL ON *.* TO ${DB_USER}@'127.0.0.1' IDENTIFIED BY '${DB_PASS}' WITH GRANT OPTION;" > /tmp/sql
echo "GRANT ALL ON *.* TO ${DB_USER}@'localhost' IDENTIFIED BY '${DB_PASS}' WITH GRANT OPTION;" >> /tmp/sql
echo "GRANT ALL ON *.* TO ${DB_USER}@'::1' IDENTIFIED BY '${DB_PASS}' WITH GRANT OPTION;" >> /tmp/sql
echo "DELETE FROM mysql.user WHERE User='';" >> /tmp/sql
echo "DROP DATABASE test;" >> /tmp/sql
echo "FLUSH PRIVILEGES;" >> /tmp/sql
cat /tmp/sql | mysql -u root --password="${DB_ROOT_PASS}"

sed -i "s|max_allowed_packet\s*=\s*1M|max_allowed_packet = ${MAX_ALLOWED_PACKET}|g" /etc/mysql/my.cnf
sed -i "s|max_allowed_packet\s*=\s*16M|max_allowed_packet = ${MAX_ALLOWED_PACKET}|g" /etc/mysql/my.cnf

#Fix for tzdata
export DEBIAN_FRONTEND=noninteractive

apt install -y zip unzip curl openssl php7.2 php7.2-common php7.2-mysql php7.2-gmp php7.2-curl php7.2-intl php7.2-mbstring php7.2-xmlrpc php7.2-gd php7.2-bcmath php7.2-xml php7.2-cli php7.2-zip

# install composer
cd /tmp 
curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

#clear cache
rm -rf /var/cache/apt/*

composer create-project --no-dev neos/neos-base-distribution /app

a2enmod rewrite
a2ensite neos
a2dissite 000-default

rm -rf /tmp/*
